package com.ngoprek.rndelasticsearch.controller;

import com.ngoprek.rndelasticsearch.document.FeatureModel;
import com.ngoprek.rndelasticsearch.services.FeatureServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@Slf4j
@RequestMapping("/features")
public class SearchController {

    private final FeatureServiceImpl featureService;

    public SearchController(FeatureServiceImpl featureService) {
        this.featureService = featureService;
    }

    @GetMapping(value = "/test")
    public String test(){
        log.info("API /TEST CALL : {} ");
        return "Sucess";
    }

    @GetMapping("/{id}")
    public Optional<FeatureModel> getById(@PathVariable String id) throws Exception {
        log.info("GET DATA BY ID : {} "+id);
        return featureService.getFeatureById(Long.parseLong(id));
    }

    @PostMapping("/_bulk")
    public List<FeatureModel> insertBulk(@RequestBody List<FeatureModel> featureModels) {
        log.info("process inserbulk : {} "+featureModels.size()+" data");
        return (List<FeatureModel>) featureService.insertBulk(featureModels);
    }

    @GetMapping("/_search/{name}")
    public List<FeatureModel> findAllByName(@PathVariable String name) {
        log.info("/name/ {} "+ name);
        return featureService.getFeatureByName(name);
    }

    @GetMapping("/_search/like/{keyword}")
    public List<FeatureModel> findAllByNameAnnotations(@PathVariable String keyword) {
        log.info("/like/ {} "+ keyword);
        return featureService.getFeatureByNameUsingWildcard(keyword);
    }

    @GetMapping("/_search/tag/{keyword}")
    public List<FeatureModel> findFeatureByTagKeyword(@PathVariable String keyword) {
        log.info("/like/ {} "+ keyword);
        return featureService.getFeatureByTag(keyword);
    }


    @GetMapping("/_search/query/{keyword}")
    public List<FeatureModel> getlListFeatureByMultiMatchQuery(@PathVariable String keyword) {
        log.info("/like/ {} "+ keyword);
        return featureService.getListFeature(keyword);
    }

    @GetMapping("/_search/wildcard/")
    public List<FeatureModel> getlListFeatureUsingWildCardQuery(@RequestParam(value = "q", required = false) String keyword) {
        log.info("/like/ {} "+ keyword);
        return featureService.getListFeatureUsingWildcard(keyword);
    }


}
