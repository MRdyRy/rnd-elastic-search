package com.ngoprek.rndelasticsearch.load;

import com.ngoprek.rndelasticsearch.document.FeatureModel;
import com.ngoprek.rndelasticsearch.repository.FeatureRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
@Slf4j
public class ESinitLoad implements CommandLineRunner {
    @Autowired
    FeatureRepository featureRepository;

    @Value("${load.init.data}")
    private Boolean loadInitData;

    @Override
    public void run(String... args) throws Exception {
        log.info("CommandLineRunner#run(String[])");
        try {
            if (loadInitData) {
                FeatureModel featureModel = new FeatureModel();
                featureModel.setId(4L);
                featureModel.setFeatureName("electric billpayment");
                featureModel.setTags(new String[]{"billpayment","electric","pln"});
                featureModel.setValue("/feature/electric-billpayment");
                featureRepository.save(featureModel);

                featureModel = new FeatureModel();
                featureModel.setId(5L);
                featureModel.setFeatureName("data purchase");
                featureModel.setTags(new String[]{"data","internet","paket","purchase","browsing"});
                featureModel.setValue("/feature/data purchase");
                featureRepository.save(featureModel);

                featureModel = new FeatureModel();
                featureModel.setId(6L);
                featureModel.setFeatureName("insurance billpayment");
                featureModel.setTags(new String[]{"asuransi","insurance","bill","payment","bpjs"});
                featureModel.setValue("/feature/insurance-billpayment");
                featureRepository.save(featureModel);
            }
        } catch (Throwable t) {
            log.error(t.toString());
        }

        if (args.length > 0) {
            log.info("first command line parameter: '{}'", args[0]);
        }
    }
}
