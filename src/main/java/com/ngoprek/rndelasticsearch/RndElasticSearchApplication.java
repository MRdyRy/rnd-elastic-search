package com.ngoprek.rndelasticsearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RndElasticSearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(RndElasticSearchApplication.class, args);
	}

}
