package com.ngoprek.rndelasticsearch.config;

import lombok.RequiredArgsConstructor;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;

@Configuration
@RequiredArgsConstructor
public class ElasticSearchConfig extends AbstractElasticsearchConfiguration {

    @Value(value = "${spring.elasticsearch.rest.uris}")
    private String hostAndPort;

    @Value(value = "${spring.elasticsearch.rest.connection-timeout}")
    private String connectTimeOut;

    @Value(value = "${spring.elasticsearch.rest.socket-timeout}")
    private String socketTimeOut;

    @Override
    @Bean
    public RestHighLevelClient elasticsearchClient() {
        final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
                .connectedTo(hostAndPort)
                .withConnectTimeout(Long.parseLong(connectTimeOut))
                .withSocketTimeout(Long.parseLong(socketTimeOut))
                .build();

        return RestClients.create(clientConfiguration).rest();
    }
}
