package com.ngoprek.rndelasticsearch.services;

import com.ngoprek.rndelasticsearch.document.FeatureModel;

import java.util.List;
import java.util.Optional;

public interface FeatureService {
    FeatureModel createFeature(FeatureModel featureModel);

    Optional<FeatureModel> getFeatureById(Long id);

    Iterable<FeatureModel> insertBulk(List<FeatureModel> featureModels);

    List<FeatureModel> getFeatureByName(String featureName);

    List<FeatureModel> getFeatureByNameUsingWildcard(String keyword);
}
