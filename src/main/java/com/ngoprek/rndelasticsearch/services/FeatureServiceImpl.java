package com.ngoprek.rndelasticsearch.services;

import com.ngoprek.rndelasticsearch.document.FeatureModel;
import com.ngoprek.rndelasticsearch.repository.FeatureRepository;
import com.ngoprek.rndelasticsearch.utils.Constants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class FeatureServiceImpl implements FeatureService{

    private final FeatureRepository featureRepository;

    private ElasticsearchOperations elasticsearchOperations;

    @Autowired
    public FeatureServiceImpl(FeatureRepository featureRepository, ElasticsearchOperations elasticsearchOperations) {
        super();
        this.featureRepository = featureRepository;
        this.elasticsearchOperations = elasticsearchOperations;
    }

    @Value(value = "${es.pagesize}")
    private String pageSize;



    @Override
    public FeatureModel createFeature(FeatureModel featureModel) {

        return featureRepository.save(featureModel);
    }

    public void createFeatureIndexBulk(final List<FeatureModel> featureModels) {
        featureRepository.saveAll(featureModels);
    }

    @Override
    public Optional<FeatureModel> getFeatureById(Long id) {
        log.info("Start process getFeatureById : {} "+id);
        Optional<FeatureModel> optionalFeatureModel = featureRepository.findById(id);
        log.info("is present : ? "+optionalFeatureModel.isPresent());
        return optionalFeatureModel;
    }

    @Override
    public Iterable<FeatureModel> insertBulk(List<FeatureModel> featureModels) {
        return featureRepository.saveAll(featureModels);
    }

    @Override
    public List<FeatureModel> getFeatureByName(String featureName) {
        log.info("Start process getFeatureByName : {} "+featureName);
        return featureRepository.findByFeatureName(featureName);
    }

    @Override
    public List<FeatureModel> getFeatureByNameUsingWildcard(String keyword) {
        log.info("Start process getFeatureByNameUsingWildcard : {} "+keyword);
        List<FeatureModel> featureModelList = featureRepository.findByFeatureNameContaining(keyword);
        log.info("result data : "+featureModelList!=null?featureModelList.size()+"":"0");
        return featureModelList;
    }

    public List<FeatureModel> getFeatureByTag(String keyword) {
        log.info("Start process getFeatureByTag : {} "+keyword);
        List<FeatureModel> featureModelList = featureRepository.findByTagsContaining(keyword);
        log.info("result data : "+featureModelList!=null?featureModelList.size()+"":"0");
        return featureModelList;
    }

    public List<FeatureModel> getListFeature( final String query){
        log.info("run services get list feature using querybuilder: {}"+query);
        QueryBuilder queryBuilder = QueryBuilders.multiMatchQuery(query,"featureName","tags")
                .fuzziness(Fuzziness.AUTO);
        Query searchQuery =  new NativeSearchQueryBuilder()
                .withFilter(queryBuilder)
                .build();
        SearchHits<FeatureModel> featureHits = elasticsearchOperations.
                search(searchQuery,FeatureModel.class, IndexCoordinates.of(Constants.FEATURE_INDEX));

        List<FeatureModel> featureModelMatch = new ArrayList<>();
        featureHits.forEach(featureModelSearchHit -> featureModelMatch.add(featureModelSearchHit.getContent()));

        return featureModelMatch;
    }


    public List<FeatureModel> getListFeatureUsingWildcard(final String query){
        log.info("run services get list feature using wildcard: {}"+query);
        QueryBuilder queryBuilder = QueryBuilders.wildcardQuery("featureName","*"+query+"*");
        Query searchQuery =  new NativeSearchQueryBuilder()
                .withFilter(queryBuilder)
                .withPageable(Pageable.ofSize(Integer.parseInt(pageSize)))
                .build();
        SearchHits<FeatureModel> featureHits = elasticsearchOperations.
                search(searchQuery,FeatureModel.class, IndexCoordinates.of(Constants.FEATURE_INDEX));

        List<FeatureModel> featureModelMatch = new ArrayList<>();
        featureHits.forEach(featureModelSearchHit -> featureModelMatch.add(featureModelSearchHit.getContent()));

        return featureModelMatch;
    }
}
