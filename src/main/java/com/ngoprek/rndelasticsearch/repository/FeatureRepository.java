package com.ngoprek.rndelasticsearch.repository;

import com.ngoprek.rndelasticsearch.document.FeatureModel;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FeatureRepository extends ElasticsearchRepository<FeatureModel,Long> {

    List<FeatureModel> findByFeatureName(String featureName);

    List<FeatureModel> findByFeatureNameContaining(String featureName);

    List<FeatureModel> findByTagsContaining(String tag);
}
