package com.ngoprek.rndelasticsearch.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Similarity;

import static org.springframework.data.elasticsearch.annotations.FieldType.Keyword;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(indexName = "features", shards = 1, replicas = 0, refreshInterval = "5s", createIndex = false)
public class FeatureModel {

    @Id
    @Field(type = FieldType.Long)
    private Long id;
    @Field(type = FieldType.Text)
    private String featureName;
    @Field(type = Keyword)
    private String[] tags;
    @Field(type = Keyword)
    private String value;
}
